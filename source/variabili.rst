﻿#########
VARIABILI
#########
Una variabile (o contatore) è un elemento capace di contenere dei dati suscettibili di modifica nel corso dell'esecuzione dei cicli di marcatura.

Ogni contatore  viene identificato da una chiave che l'utente deve inserire nell'apposito campo. Le chiavi devono essere univoche.
Per collegare il contatore ad un elemento grafico che contiene un testo, bisogna fare riferimento alla chiave.

Per esempio, per collegare un contatore con chiave “key1” ad un testo questo deve contenere la chiave scritta in questo formato **#[key1]#**. 
Ogni testo può contenere più di un contatore con anche dei campi fissi, basterà semplicemente richiamare più chiavi: ad esempio si possono avere “commessa n° #[key1]# - ordine di lavoro #[key2]#”.

Ogni variabile può essere utilizzata anche più di una volta dentro lo stesso progetto, ma solo all'avvio marcatura questa verrà rielaborata.

I contatori sono forniti di un'abilitazione, se questa non è attiva, non vengono gestiti e non compaiono sul foglio di lavoro e in marcatura.

*******************
CREAZIONE VARIABILE
*******************
Per creare una variabile:

* Definirne il nome. Il nome può essere qualunque si voglia, ad esempio **#[var2]#**;
* Inserire in un Testo Lineare il nome tra la seguente combinazione di caratteri **#[ ]#**;
* Inserire una nuova variabile nella finestra Proprietà, schiacciando sull'icona "Aggiungi contatore".

.. _variabili_aggiungi_variabile:
.. figure:: _static/variabili_aggiungi_variabile.png
   :width: 10 cm
   :align: center

****************************
IMPOSTAZIONE DELLA VARIABILE
****************************

.. _variabili_impostazioni_variabile:
.. figure:: _static/variabili_impostazioni_variabile.png
   :width: 10 cm
   :align: center

   Impostazioni *variabile*

Prima di eseguire la marcatura è necessario compilare i seguenti campi della finestra :guilabel:`Proprietà`:

* :kbd:`Ab` (abilita l'utilizzo del contatore);
* :kbd:`Tipo` (selezionare tra Contatore numerico incrementale - Date e orari - Testo);
* :kbd:`Chiave` (inserire il nome della variabile da trasformare in contatore, ad esempio var2 se abbiamo #[var2]# nel file);
* :kbd:`Formato` (consente di decidere la disposizione dei numeri che si intendono visualizzare);
* :kbd:`Inizio` (valore iniziale del contattore una volta che si resetta);
* :kbd:`Corrente` (valore attuale del contatore);
* :kbd:`Fine` (valore massimo che il contatore più raggiungere prima del reset automatico);
* :kbd:`Passo` (differenza fra due valori consecutivi).

Per maggiori delucidazioni sul tipo di variabile e suo formato si consiglia di vedere :ref:`SCHEDA_DI_APPROFONDIMENTO_-_Tipologia_Variabili`.

La variabile è testo lineare. Tutte le caratteristiche (font, dimensione, posizione, rotazione ecc...) possono essere definite.

MARCATURA DATAMATRIX
--------------------

* Settare :kbd:`altezza Z[mm]` del punto di marcatura nella finestra Struttura Progetto e premere :kbd:`GO`.
* Selezionare i parametri di marcatura nella finestra Livelli.
* Premere :kbd:`ROSSO` per centrare l'area di marcatura sul pezzo.
* Chiudere lo sportello.
* Premere :kbd:`START` per eseguire la marcatura.

.. _SCHEDA_DI_APPROFONDIMENTO_-_Tipologia_Variabili:

SCHEDA DI APPROFONDIMENTO - Tipologia Variabili
===============================================

* **Contatore numerico incrementale** (ad ogni marcatura il valore “corrente” viene incrementato del “passo”. Il campo “formato” è uno strumento molto potente, consente di cambiare totalmente la disposizione dei numeri che si intendono visualizzare, alcuni esempio sono forniti di seguito:)

.. _variabili_campo_formato:
.. figure:: _static/variabili_campo_formato.png
   :width: 10 cm
   :align: center

   Campo *formato*

* **Date e orari** (anche in questo caso il valore viene elaborato ad ogni marcatura ed è collegato al valore di tempo corrente del computer. Come per i contatori incrementali, anche in questo caso è presente il campo “formato” per poter costruire la stringa di testo desiderata. In questo caso le possibilità sono veramente tante e di seguito sono riportati alcuni esempi)

.. _variabili_campo_data:
.. figure:: _static/variabili_campo_data.png
   :width: 10 cm
   :align: center

   Campo *data*

* **Testi** (questa tipologia non rappresenta un vero e proprio contatore in quanto viene solamente fatte la sostituzione della chiave con una stringa di testo. Non ci sono formati da applicare e la sostituzione viene fatta in modo diretto. Apparentemente si potrebbe dire che questo oggetto non porta alcun vantaggio perché non ha bisogno di aggiornamenti o rappresentazioni personalizzate. Nella pratica invece questo oggetto viene molto usato in presenza di copie multiple: collegando tutte le copie a questo tipo di “contatore”, sarà un semplice aggiornamento al campo per aggiornare tutte le copie).
