.. PRISMA documentation master file, created by
   sphinx-quickstart on Tue Jan 7 13:21:25 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#######################
Manuale Software Prisma
#######################

.. toctree::
   :maxdepth: 3
   :numbered:

   pittogrammi
   installazione
   architettura_di_sistema
   impostazioni_applicazione
   impostazioni_percorsi
   impostazioni_interfaccia
   impostazioni_utenti
   impostazioni_notifiche
   impostazioni_DB_commesse
   impostazioni_profinet
   menù_prisma
   test_parametri
   test_pfs
   test_focus
   test_3D
   interfaccia_grafica
   nuovo_progetto
   impostazioni_livelli
   impostazioni_laser
   impostazioni_struttura_progetto
   marcatura
   barcode
   datamatrix
   foglio_dati
   variabili
   marcatura_3D
   strumenti_livelli
   cambio_focale
   auto_caricamento_progetto
   caricamento_barcode
   impostazioni_accessori
   note
