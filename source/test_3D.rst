.. _test_3D:

#######
3D TEST
#######
*Inserire descrizione*

.. _3D_test:
.. figure:: _static/3D_test.png
   :width: 14 cm
   :align: center

   Finestra *3D TEST*

Per eseguire il test impostare:

* :kbd:`Passo inclinazione [°]` *Inserire descrizione*;
* :kbd:`Ordine casuale` *Inserire descrizione*;
* :kbd:`Numero di Livelli` *Inserire descrizione*;
* :kbd:`Profondità Finale [mm]` *Inserire descrizione*;
* :kbd:`Sollevatore` *Inserire descrizione*;
* :kbd:`Profondità File [mm]` *Inserire descrizione*;
* :kbd:`Start Z [mm]` *Inserire descrizione*;
* :kbd:`Dimensione Quadrato [mm]` *Inserire descrizione*;
* :kbd:`Passo X [mm]` *Inserire descrizione*;
* :kbd:`Passo Y [mm]` *Inserire descrizione*.

.. |aggiungi_elemento| image:: _static/3D_test_aggiungi_elemento.png
   :width: 1 cm
   :align: middle

.. |genera_matrice| image:: _static/3D_test_genera_matrice.png
   :width: 1 cm
   :align: middle

.. |rimuovi_elemento| image:: _static/3D_test_rimuovi_elemento.png
   :width: 1 cm
   :align: middle

.. |esporta_livello| image:: _static/3D_test_esporta_livello.png
   :width: 1 cm
   :align: middle

* Impostare il numero di :kbd:`Riga` e :kbd:`Colonna`;
* Premere il pulsante |aggiungi_elemento| oppure il pulsante |genera_matrice| per far apparire una ulteriore riga di configurazione;

* Impostare i relativi parametri:
   * :kbd:`Potenza`;
   * :kbd:`Frequenza`;
   * :kbd:`Velocità`;
   * :kbd:`Bid`;
   * :kbd:`Spaziatura`;
   * :kbd:`LS`;
   * :kbd:`Forma d'onda`;
* Per eliminare una riga premere il pulsante |rimuovi_elemento|;
* Per esportare un livello premere il pulsante |esporta_livello|.

* Premere :kbd:`AVVIA ROSSO` per centrare l'area di marcatura sulla piastrina.
* Chiudere lo sportello.
* Premere :kbd:`AVVIA LASER` per eseguire la marcatura.
